DFPlayer
========

.. automodule:: devices.dfplayer.dfplayer
    :members:
    :undoc-members:
    :show-inheritance:
    :private-members:
