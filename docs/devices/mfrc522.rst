MFRC 522
========

.. automodule:: devices.mfrc522.mfrc522
    :members:
    :undoc-members:
    :show-inheritance:
