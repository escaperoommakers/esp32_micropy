# coding: utf-8

import utime

from shift_register import ShiftRegister, ShiftRegisterPin

shift_register = ShiftRegister(
    data_pin=23,
    latch_pin=21,
    clock_pin=22,
    num_outs=8,
)

pins = [ShiftRegisterPin(shift_register, pin) for pin in range(8)]

counter = 0
while True:
    pins[counter % 8].value(1)
    utime.sleep(1)
    pins[counter % 8].value(0)
    utime.sleep(1)
    counter += 1