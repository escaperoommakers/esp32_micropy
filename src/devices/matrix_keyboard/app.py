import utime

from matrix_keyboard import MatrixKeyboard


class App:
    def __init__(self, client, topic):
        self.client = client
        self.topic = topic
        self.keyboard = MatrixKeyboard()

    def on_message(self, topic, message):
        print(topic)
        print(message)

    def start(self):
        pass

    def loop(self):
        self.keyboard.read_keys()
        print(self.keyboard.sequence)
        time.sleep_ms(1000)
