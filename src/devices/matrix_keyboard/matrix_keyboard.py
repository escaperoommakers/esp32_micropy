# coding: utf-8

"""Module for operating with matrix keyboard."""
import utime

from machine import Pin

DECODER = {
    '1': 0,
    '2': 1,
    '3': 2,
    'A': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    'B': 7,
    '7': 8,
    '8': 9,
    '9': 10,
    'C': 11,
    '*': 12,
    '0': 13,
    '#': 14,
    'D': 15,
}

ENCODER = [
    '1',
    '2',
    '3',
    'A',
    '4',
    '5',
    '6',
    'B',
    '7',
    '8',
    '9',
    'C',
    '*',
    '0',
    '#',
    'D',
]

DEFAULT_ROWS = [35, 34, 39, 36]
DEFAULT_COLS = [33, 25, 26, 32]


class MatrixKeyboard:
    """Module for WINGONEER 4x4 keyboard or another keyboard."""

    def __init__(self, rows=DEFAULT_ROWS, cols=DEFAULT_COLS):
        """
        Init keyboard.

        param rows: row pins from top to bottom
                    default [35, 34, 39, 36]
        param cols: col pins from left to right
                    default [33, 25, 26, 32]
        """
        self.rows = [Pin(row, Pin.IN) for row in rows]
        self.cols = [Pin(col, Pin.OUT) for col in cols]
        self.range = range(16)
        self.last_states = [1 for _ in self.range]
        self.states = [1 for _ in self.range]
        self.sequence = []

    def read_keys(self):
        """Read all keys."""
        for index in self.range:
            self.last_states[index] = self.states[index]
            self.read_key(index)

    def read_key(self, index):
        """Read key with specific index."""
        self.set_col(index % 4)
        self.states[index] = self.rows[index // 4].value()
        if self.last_states[index] != self.states[index]:
            if self.states[index] == 0:
                utime.sleep_ms(10)
                self.states[index] = self.rows[index // 4].value()
                if self.states[index] == 0:
                    self.sequence.append(index)

    def set_col(self, col):
        """Set active column."""
        self.cols[0].value(1)
        self.cols[1].value(1)
        self.cols[2].value(1)
        self.cols[3].value(1)
        self.cols[col].value(0)
