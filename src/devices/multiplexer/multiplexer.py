# coding: utf-8

"""Module for interaction with multiplexer."""

from machine import ADC, Pin


class Multiplexer:
    """
    Class for one multiplexer connected to ADC pin.

    :param input_pin: multiplexers input.
    :param address_pins: multiplexer address pins.

    """

    def __init__(self, input_pin, address_pins):
        """Initialize multiplexer cluster."""
        self.input_pin = ADC(Pin(input_pin))
        self.address_pins = []
        for pin in address_pins:
            self.address_pins.append(Pin(pin, Pin.OUT))
        self.multiplexer_size = 1 << len(self.address_pins)
        self.states = [0 for _ in range(self.multiplexer_size)]
        self._set_number(0)

    def get_states(self):
        """
        Return multiplexer inputs states list.

        :return: Inputs states.
        """
        return self.states

    def read_states(self):
        """Read states of multiplexer inputs to states list."""
        for multiplexer_index in range(self.multiplexer_size):
            self._set_number(multiplexer_index)
            self.states[multiplexer_index] = self._read()

    def set_number(self, out_number):
        """Select input pin."""
        for pin_index in range(len(self.address_pins) - 1, -1, -1):
            self.address_pins[pin_index].value(out_number & 1)
            out_number >>= 1

    def read(self):
        """
        Read selected pin.

        :return: Selected pin value.
        """
        return self.input_pin.read()


class MultiplexerCluster:
    """
    Class for many multiplexers on one group of address pins.

    :param input_pins: cluster multiplexers inputs.
    :param address_pins: cluster multiplexers address pins.

    """

    def __init__(self, input_pins, address_pins):
        """Initialize multiplexer cluster."""
        self.input_pins = [ADC(Pin(pin)) for pin in input_pins]
        self.address_pins = []
        for pin in address_pins:
            self.address_pins.append(Pin(pin, Pin.OUT))
        self.multiplexer_size = 1 << len(self.address_pins)
        self.cluster_size = len(input_pins)
        self.states = []
        for _ in range(self.cluster_size):
            self.states.append([0 for _ in range(self.multiplexer_size)])

        self.set_number(0)

    def get_states(self):
        """
        Return list of multiplexers inputs states lists.

        :return: Inputs states.
        """
        return self.states

    def read_states(self):
        """Read states of multiplexers inputs to states lists."""
        for multiplexer_index in range(self.multiplexer_size):
            self.set_number(multiplexer_index)
            for input_index in range(self.cluster_size):
                input_value = self.input_pins[input_index].read()
                self.states[input_index][multiplexer_index] = input_value

    def set_number(self, out_number):
        """Select input pin."""
        for pin_index in range(len(self.address_pins) - 1, -1, -1):
            self.address_pins[pin_index].value(out_number & 1)
            out_number >>= 1

    def read(self, input_index):
        """
        Read selected pin from ``input_index`` input.

        :param input_index: index of multiplexer in cluster.

        :return: Selected pin value.
        """
        return self.input_pins[input_index].read()


class MultiplexerPin:
    """
    Multiplexer digital pin emulator.

    :param multiplexer: multiplexer of pin.
    :param position: position of pin in multiplexer.
    :param pull: type of actual (physycal) pull (Pin.PULL_UP or Pin.PULL_DOWN).
    :param treshold: treshold of changing digital level.

    """

    def __init__(self, multiplexer, position, pull=None, treshold=512):
        """Initialize multiplexer cluster."""
        self.multiplexer = multiplexer
        self.actual_pull = pull
        self.position = position
        self.treshold = treshold

    def init(
        self,
        mode=-1,
        pull=-1,
        value=-1,
        drive=-1,
        alt=-1,
    ):
        """
        Pin.init method fake.

        It can set mode to Pin.IN.
        It can set value if present.
        It can set virtual pull if present.

        See machine.Pin constructor documentation for details
        of the mode argument.
        """
        if mode != -1 and mode != Pin.IN:
            raise ValueError('Mode of MultiplexerPin can be only Pin.IN')
        if pull != -1:
            self.pull = pull
        if value != -1:
            self.value(value)

    def __call__(self, value=None):
        """
        MultiplexerPin objects are callable.

        The call method provides a (fast) shortcut
        to set and get the value of the pin.
        It is equivalent to MultiplexerPin.value().
        See MultiplexerPin.value() for more details.
        """
        return self.value(value)

    def value(self, value=None):
        """
        Set and get the value of the pin.

        It depends on whether the argument ``value`` is supplied or not.

        This pin unsupport set and raise exception.
        """
        self.multiplexer.set_number(self.position)
        if value:
            raise ValueError('MultiplexerPin can\'t be setted')
        if self.actual_pull is None and self.pull is not None:
            raise ValueError('MultiplexerPin misconfigured')
        if self.pull is None and self.actual_pull is not None:
            raise ValueError('MultiplexerPin misconfigured')
        if self.actual_pull == self.pull:
            return int(self.treshold <= self.multiplexer.read())
        return int(self.treshold >= self.multiplexer.read())

    def read(self):
        """
        Call read of internal ADC.

        :return: analog value of pin.
        """
        self.multiplexer.set_number(self.position)
        return self.multiplexer.read()

    def on(self):
        """
        Set pin to “1” output level.

        This pin unsupport set and raise exception.
        """
        raise ValueError('MultiplexerPin can\'t be setted')

    def off(self):
        """
        Set pin to “0” output level.

        This pin unsupport set and raise exception.
        """
        raise ValueError('MultiplexerPin can\'t be setted')

    def mode(self, mode=None):
        """
        Get or set the pin mode.

        See machine.Pin constructor documentation
        for details of the mode argument.

        Mode of MultiplexerPin can be only Pin.IN otherwise
        raise exception.
        """
        if not mode:
            return Pin.IN
        if mode != Pin.IN:
            raise ValueError('Mode of MultiplexerPin can be only Pin.IN')

    def pull(self, pull=None):
        """
        Get or set the pin (virtual) pull state.

        See machine.Pin constructor documentation
        for details of the mode argument.

        If actual pull is not eqivalent to virtual pull
        then digital logic is inverted. If pin has no actual
        physical pull the both pull shoutld be ``None``.
        """
        if not pull:
            return None
        self.pull = pull


class MultiplexerClusterPin:
    """
    Multiplexer cluster digital pin emulator.

    :param multiplexer_cluster: multiplexer cluster.
    :param multiplexer: multiplexer index in cluster.
    :param position: position of pin in multiplexer.
    :param pull: type of actual (physycal) pull (Pin.PULL_UP or Pin.PULL_DOWN).
    :param treshold: tresholld of changing digital level.

    """

    def __init__(
        self,
        multiplexer_cluster,
        multiplexer,
        position,
        pull=None,
        treshold=512,
    ):
        """Initialize multiplexer cluster pin."""
        self.multiplexer_cluster = multiplexer_cluster
        self.actual_pull = pull
        self.multiplexer = multiplexer
        self.position = position
        self.treshold = treshold

    def init(
        self,
        mode=-1,
        pull=-1,
        value=-1,
        drive=-1,
        alt=-1,
    ):
        """
        Pin.init method fake.

        It can set mode to Pin.IN.
        It can set value if present.
        It can set virtual pull if present.

        See machine.Pin constructor documentation for details
        of the mode argument.
        """
        if mode != -1 and mode != Pin.IN:
            raise ValueError('Mode of MultiplexerClusterPin can be only Pin.IN')
        if pull != -1:
            self.pull = pull
        if value != -1:
            self.value(value)

    def __call__(self, value=None):
        """
        MultiplexerClusterPin objects are callable.

        The call method provides a (fast) shortcut
        to set and get the value of the pin.
        It is equivalent to MultiplexerClusterPin.value().
        See MultiplexerClusterPin.value() for more details.
        """
        return self.value(value)

    def value(self, value=None):
        """
        Set and get the value of the pin.

        It depends on whether the argument ``value`` is supplied or not.

        This pin unsupport set and raise exception.
        """
        self.multiplexer_cluster.set_number(self.position)
        if value:
            raise ValueError('MultiplexerClusterPin can\'t be setted')
        if self.actual_pull is None and self.pull is not None:
            raise ValueError('MultiplexerClusterPin misconfigured')
        if self.pull is None and self.actual_pull is not None:
            raise ValueError('MultiplexerClusterPin misconfigured')
        if self.actual_pull == self.pull:
            return int(self._read_treshold())
        return int(self._read_treshold())

    def read(self):
        """
        Call read of internal ADC.

        :return: analog value of pin.
        """
        self.multiplexer.set_number(self.position)
        return self.multiplexer.read(self.multiplexer)

    def on(self):
        """
        Set pin to “1” output level.

        This pin unsupport set and raise exception.
        """
        raise ValueError('MultiplexerClusterPin can\'t be setted')

    def off(self):
        """
        Set pin to “0” output level.

        This pin unsupport set and raise exception.
        """
        raise ValueError('MultiplexerClusterPin can\'t be setted')

    def mode(self, mode=None):
        """
        Get or set the pin mode.

        See machine.Pin constructor documentation
        for details of the mode argument.

        Mode of MultiplexerClusterPin can be only
        Pin.IN otherwise raise exception.
        """
        if not mode:
            return Pin.IN
        if mode != Pin.IN:
            raise ValueError('MultiplexerClusterPin can be only Pin.IN')

    def pull(self, pull=None):
        """
        Get or set the pin (virtual) pull state.

        See machine.Pin constructor documentation
        for details of the mode argument.

        If actual pull is not eqivalent to virtual pull
        then digital logic is inverted. If pin has no actual
        physical pull the both pull shoutld be ``None``.
        """
        if not pull:
            return None
        self.pull = pull

    def _read_treshold(self):
        return self.treshold <= self.multiplexer_cluster.read(self.multiplexer)
