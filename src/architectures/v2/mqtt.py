# coding: utf-8

import ujson
from umqtt.simple import MQTTClient as StandartMQTTClient


class MQTTClient:
    def __init__(
        self,
        topic,
        client_id,
        server,
        port=0,
        user=None,
        password=None,
        keepalive=0,
        ssl=False,
        ssl_params=dict,
    ):
        self.topic = topic
        self.mqtt = StandartMQTTClient(
            client_id=client_id,
            server=server,
            port=port,
            user=user,
            password=password,
            keepalive=keepalive,
            ssl=ssl,
            ssl_params=ssl_params,
        )

        self.callback = None

    def _callback(self, topic, message):
        package = {}
        if self.topic != topic.decode('utf-8'):
            print('Incorrect topic.')
            return
        try:
            package = ujson.loads(message)
        except ValueError:
            print('Incorrect JSON.')
            print('Message: {0}'.format(message))
            return
        self.callback(package)

    def set_callback(self, callback):
        self.callback = callback
        self.mqtt.set_callback(
            lambda topic, message: self._callback(topic, message),
        )

    def subscribe(self, topic, qos=0):
        self.mqtt.subscribe(topic, qos)

    def publish(self, topic, msg, retain=False, qos=0):
        self.mqtt.publish(topic, msg, retain, qos)

    def publish_event(self, call, args=None):
        package = {}
        package['from'] = self.topic
        package['class'] = 'event'
        package['call'] = call
        if args:
            package['args'] = args
        self.mqtt_client.publish(
            'raspberry',
            ujson.dumps(package),
            qos=1,
        )
