# coding: utf-8

import gc
import time


class App:
    def __init__(self, client):
        self.client = client

    def on_message(self, package):
        print(package)
        self.client.publish('micropython', 'hello')

    def start(self):
        pass

    def loop(self):
        time.sleep_ms(1000)
        print(gc.mem_free())
