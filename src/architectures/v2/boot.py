# coding: utf-8

import network
import ujson

config_file = open('config.json')
config = ujson.loads(config_file.read())
config_file.close()

sta_if = network.WLAN(network.STA_IF)
sta_if.active(is_active=True)
sta_if.connect(config['ssid'], config['psk'])
print(sta_if.ifconfig())
