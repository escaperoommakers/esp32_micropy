# coding: utf-8

import gc
import utime


class App:
    def __init__(self, client, topic):
        self.client = client
        self.topic = topic

    def on_message(self, topic, message):
        print(topic)
        print(message)
        self.client.publish('micropython', 'hello')

    def start(self):
        pass

    def loop(self):
        utime.sleep_ms(1000)
        print(gc.mem_free())
